package com.example.alifa_ridho.fusedlocationdemo.model;

/**
 * Created by alifa_ridho on 12/9/16.
 */

public class LocationModel {
    public String id;
    public String lat;
    public String lng;

    public LocationModel() {
    }

    public LocationModel(String id, String lat, String lng) {
        this.id = id;
        this.lat = lat;
        this.lng = lng;
    }
}
