package com.example.alifa_ridho.fusedlocationdemo;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.alifa_ridho.fusedlocationdemo.component.AppComponent;
//import com.example.alifa_ridho.fusedlocationdemo.component.DaggerAppComponent;
import com.example.alifa_ridho.fusedlocationdemo.module.AppModule;

/**
 * Created by alifa_ridho on 12/8/16.
 */

public class MainApplication extends MultiDexApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
//        appComponent = DaggerAppComponent.builder()
//                .appModule(new AppModule(this))
//                .build();
    }

    private AppComponent getAppComponent(){
        return appComponent;
    }

//    public static AppComponent getAppComponent(Activity activity) {
//        return ((AppComponent) activity.getApplication()).getAppComponent();
//    }
//
//    public static AppComponent getAppComponent(Fragment fragment) {
//        return ((AppComponent) fragment.getActivity().getApplication()).getAppComponent();
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
